import java.sql.*;


/**
 *
 * @author wx
 
 */

public class Demo {
    public static void main(String[] args) throws SQLException {
        try {
		Class.forName("com.mysql.jdbc.Driver");
	} catch (ClassNotFoundException e) {
		System.out.println("Where is your MySQL JDBC Driver?");
		e.printStackTrace();
		return;
	}

        Connection myConn = null;
        Statement myStmt = null;
        ResultSet myRs = null;
        String query = "CREATE table IF NOT EXISTS employees";
 
        String user = "root";
        String pass = "";
        
        String sqltables ="CREATE TABLE if not exists employees (" +
                         "        idNo INT(64) NOT NULL ," +
                         "        initials VARCHAR(2)," +
                         "        employesdate DATE," +
                         "        salary INT(64)" +
                                 "VALUES (10,'MD','2007-05-05','15000')"+

                         "); ";

               String selecttables = "SELECT * FROM employees "; 
               String sql = "UPDATE employees SET idNo=?,initials=?, employesdate=? ,salary =? WHERE  idNo=10";
       
               String sql2 = "DELETE FROM employees WHERE idNo=?";
               try {
            // 1. Get a connection to database
           myConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/demo", user, pass);   
           Statement s = myConn.createStatement();
           
          int count = 0;
 

        ResultSet result = s.executeQuery(selecttables);
         while (result.next()){
            String idNo = result.getString(1);
            String initials = result.getString(2);
            String  employesdate = result.getString(3);
            String  salary = result.getString(4);

            String output = "User #%d: %s - %s - %s - %s";
        System.out.println(String.format(output,++count, idNo,initials,employesdate, salary));
        }
PreparedStatement statement = myConn.prepareStatement(sql);
statement.setString(1, "14");
statement.setString(2, "DL");
statement.setString(3, "2007-01-05");
statement.setString(4, "15000");

        int rowsUpdated = statement.executeUpdate();
       
        
 PreparedStatement sta = myConn.prepareStatement(sql2);
sta.setString(1, "14");
 

int rowsDeleted = sta.executeUpdate();

        } catch (Exception exc) {
            exc.printStackTrace();
        } finally {
            if (myRs != null) {
                myRs.close();
            }

            if (myStmt != null) {
                myStmt.close();
            }

            if (myConn != null) {
                myConn.close();
            }
        }
    }    

}
